#include "mbed.h"

#define HOTT_MESSAGE_PREPARATION_FREQUENCY_5_HZ ((1000 * 1000) / 5)
#define HOTT_RX_SCHEDULE 4000
#define HOTT_TX_DELAY_US 3000


#define HOTT_TEXT_MODE_REQUEST_ID       0x7f
#define HOTT_BINARY_MODE_REQUEST_ID     0x80
//Sensor Ids

//Graupner #33611 General Air Module
//#define HOTT_TELEMETRY_GAM_SENSOR_ID    0x8d
//Graupner #33600 GPS Module
#define HOTT_TELEMETRY_GPS_SENSOR_ID    0x8a
//Graupner #33620 Electric Air Module
#define HOTT_TELEMETRY_EAM_SENSOR_ID    0x8e
//Graupner #33601 Vario Module
//#define HOTT_TELEMETRY_VARIO_SENSOR_ID  0x89
//Textmode address to simulate (GPS)
#define HOTT_SIM_TEXTMODE_ADDRESS       HOTT_TELEMETRY_GPS_SENSOR_ID


#define HOTT_TEXTMODE_MSG_TEXT_LEN 168
// Text mode msgs type
struct HOTT_TEXTMODE_MSG
{
  uint8_t start_byte;      //#01 constant value 0x7b
  uint8_t fill1;           //#02 constant value 0x00
  uint8_t warning_beeps;   //#03 1=A 2=B ...
  uint8_t msg_txt[HOTT_TEXTMODE_MSG_TEXT_LEN];     //#04 ASCII text to display to
  // Bit 7 = 1 -> Inverse character display
  // Display 21x8
  uint8_t stop_byte;               //#171 constant value 0x7d
  //      int8_t parity;          //#172 Checksum / parity
};

HOTT_TEXTMODE_MSG hott_txt_msg;

typedef struct HOTT_EAM_MSG_s {
    uint8_t start_byte;          //#01 start uint8_t
    uint8_t eam_sensor_id;       //#02 EAM sensort id. constat value 0x8e
    uint8_t warning_beeps;           //#03 1=A 2=B ... or 'A' - 0x40 = 1
                                // Q    Min cell voltage sensor 1
                                // R    Min Battery 1 voltage sensor 1
                                // J    Max Battery 1 voltage sensor 1
                                // F    Mim temperature sensor 1
                                // H    Max temperature sensor 1
                                // S    Min cell voltage sensor 2
                                // K    Max cell voltage sensor 2
                                // G    Min temperature sensor 2
                                // I    Max temperature sensor 2
                                // W    Max current
                                // V    Max capacity mAh
                                // P    Min main power voltage
                                // X    Max main power voltage
                                // O    Min altitude
                                // Z    Max altitude
                                // C    (negative) sink rate m/sec to high
                                // B    (negative) sink rate m/3sec to high
                                // N    climb rate m/sec to high
                                // M    climb rate m/3sec to high

    uint8_t sensor_id;           //#04 constant value 0xe0
    uint8_t alarm_invers1;       //#05 alarm bitmask. Value is displayed inverted
                                //Bit#  Alarm field
                                // 0    mAh
                                // 1    Battery 1
                                // 2    Battery 2
                                // 3    Temperature 1
                                // 4    Temperature 2
                                // 5    Altitude
                                // 6    Current
                                // 7    Main power voltage
    uint8_t alarm_invers2;       //#06 alarm bitmask. Value is displayed inverted
                                //Bit#  Alarm Field
                                // 0    m/s
                                // 1    m/3s
                                // 2    Altitude (duplicate?)
                                // 3    m/s (duplicate?)
                                // 4    m/3s (duplicate?)
                                // 5    unknown/unused
                                // 6    unknown/unused
                                // 7    "ON" sign/text msg active

    uint8_t cell1_L;             //#07 cell 1 voltage lower value. 0.02V steps, 124=2.48V
    uint8_t cell2_L;             //#08
    uint8_t cell3_L;             //#09
    uint8_t cell4_L;             //#10
    uint8_t cell5_L;             //#11
    uint8_t cell6_L;             //#12
    uint8_t cell7_L;             //#13
    uint8_t cell1_H;             //#14 cell 1 voltage high value. 0.02V steps, 124=2.48V
    uint8_t cell2_H;             //#15
    uint8_t cell3_H;             //#16
    uint8_t cell4_H;             //#17
    uint8_t cell5_H;             //#18
    uint8_t cell6_H;             //#19
    uint8_t cell7_H;             //#20

    uint8_t batt1_voltage_L;     //#21 battery 1 voltage lower value in 100mv steps, 50=5V. optionally cell8_L value 0.02V steps
    uint8_t batt1_voltage_H;     //#22

    uint8_t batt2_voltage_L;     //#23 battery 2 voltage lower value in 100mv steps, 50=5V. optionally cell8_H value. 0.02V steps
    uint8_t batt2_voltage_H;     //#24

    uint8_t temp1;               //#25 Temperature sensor 1. 20=0�, 46=26� - offset of 20.
    uint8_t temp2;               //#26 temperature sensor 2

    uint8_t altitude_L;          //#27 Attitude lower value. unit: meters. Value of 500 = 0m
    uint8_t altitude_H;          //#28

    uint8_t current_L;           //#29 Current in 0.1 steps
    uint8_t current_H;           //#30

    uint8_t main_voltage_L;      //#31 Main power voltage (drive) in 0.1V steps
    uint8_t main_voltage_H;      //#32

    uint8_t batt_cap_L;          //#33 used battery capacity in 10mAh steps
    uint8_t batt_cap_H;          //#34

    uint8_t climbrate_L;         //#35 climb rate in 0.01m/s. Value of 30000 = 0.00 m/s
    uint8_t climbrate_H;         //#36

    uint8_t climbrate3s;         //#37 climbrate in m/3sec. Value of 120 = 0m/3sec

    uint8_t rpm_L;               //#38 RPM. Steps: 10 U/min
    uint8_t rpm_H;               //#39

    uint8_t electric_min;        //#40 Electric minutes. Time does start, when motor current is > 3 A
    uint8_t electric_sec;        //#41

    uint8_t speed_L;             //#42 (air?) speed in km/h. Steps 1km/h
    uint8_t speed_H;             //#43
    uint8_t stop_byte;           //#44 stop uint8_t
} HOTT_EAM_MSG_t;

HOTT_EAM_MSG_t hott_eam_msg;


bool HOTT_REQ_UPDATE_EAM = false;



RawSerial uart(PA_9, PA_10); // tx, rx
Serial pc(USBTX, USBRX);

Ticker ticker; // !! max ~30 [min] <- 32-bit [us] counter
Timer timer; // !! max ~30 [min] <- 32-bit [us] counter

volatile uint32_t tnow = 0;
uint32_t last_message_prepared_at = 0;

const uint32_t rx_buffer_size = 255;
const uint32_t tx_buffer_size = 255;
volatile uint8_t rx_buffer[rx_buffer_size+1];
volatile uint8_t tx_buffer[rx_buffer_size+1];
volatile uint8_t rx_in = 0;
volatile uint8_t rx_out = 0;
volatile uint8_t tx_in = 0;
volatile uint8_t tx_out = 0;

bool hott_telemetry_is_sending = false;
uint32_t hott_serial_timer;
uint32_t last_hott_check_request = 0;
bool looking_for_request = true;
uint8_t *hott_msg_ptr = 0;
uint8_t hott_msg_len = 0;
uint8_t hott_telemetry_sendig_msgs_id = 0;
uint8_t msg_crc = 0;

#define HOTT_CRC_SIZE (sizeof(msg_crc))

void Rx_interrupt();
void Tx_interrupt();
void hott_enable_transmitter();  
void hott_enable_receiver(); 
void init();
uint32_t binary(unsigned char n);
void send_usb_msg();
void hott_serial_scheduler();
void hott_check_serial_data();
int hott_port_available();
uint32_t hott_port_read();
void hott_port_write(uint8_t);
void hott_port_flush();
void hott_clear_text_screen();
void hott_send_text_msg();
void hott_send_eam_msg();
void hott_update_eam_msg();
void hott_update_telemetry_data();
void hott_send_msg(uint8_t *buffer, int len);
void hott_send_telemetry_data();

void HOTT_PrintWord(uint8_t pos,  char *w, bool inverted);

int main()
{
  init();
  
  while(1)
    {
      // do nothing;
    }

  return 0;
}


void init()
{
  // Setup hott port
  uart.baud(19200);
  hott_enable_receiver();

  // Init text msg
  memset(&hott_txt_msg, 0, sizeof(struct HOTT_TEXTMODE_MSG));
  hott_txt_msg.start_byte = 0x7b;
  hott_txt_msg.fill1 = 0x00;
  hott_txt_msg.warning_beeps = 0x00;
  hott_txt_msg.stop_byte = 0x7d;

  // Init EAM msg
  memset(&hott_eam_msg, 0, sizeof(hott_eam_msg));
  hott_eam_msg.start_byte = 0x7c;
  hott_eam_msg.eam_sensor_id = HOTT_TELEMETRY_EAM_SENSOR_ID;
  hott_eam_msg.sensor_id = 0xe0;
  hott_eam_msg.stop_byte = 0x7d;
 

  timer.start();
  ticker.attach(&hott_serial_scheduler,1e-3); // time in [s]
  //  ticker.attach(&send_usb_msg,1e-3); // time in [s]

  //  NVIC_SetPriority(TIMER3_IRQn, 10);
}

// Interupt Routine to read in data from serial port
void Rx_interrupt()
{
  // Loop just in case more than one character is in UART's receive FIFO buffer
  // Stop if buffer full
  while (uart.readable())
    {
      rx_buffer[rx_in] = uart.getc();
      //      pc.printf("%d %d\n",rx_in,rx_out);
      // pc.printf("%x\n",rx_buffer[rx_in]);
      rx_in = (rx_in + 1) % rx_buffer_size;
    }
}

// Interupt Routine to write out data to serial port
void Tx_interrupt()
{
  // Loop to fill more than one character in UART's transmit FIFO buffer
  // Stop if buffer empty
  while ((uart.writeable()) && (tx_in != tx_out))
    {
      uart.putc(tx_buffer[tx_out]);
      tx_out = (tx_out + 1) % tx_buffer_size;
    }
}

uint32_t binary(unsigned char n)
{
  uint32_t binary = 0;
  uint32_t ten_to_the_i = 1;

  while(n) {
    if (n & 0x1)
      binary += ten_to_the_i;
    ten_to_the_i *= 10;
    n >>= 1;
  }
  
  return binary;
}

void send_usb_msg()
{
  tnow = timer.read_us();
  pc.printf("%d\n",tnow);
}

// Called periodically (≈1ms) by timer scheduler
void hott_serial_scheduler()
{
  tnow = timer.read_us();
  
  bool should_prepare_hott_messages = (tnow - last_message_prepared_at) >= HOTT_MESSAGE_PREPARATION_FREQUENCY_5_HZ;
  
  if(should_prepare_hott_messages)
    {
      hott_update_telemetry_data();
      last_message_prepared_at = tnow;
    }
  
  if(!hott_telemetry_is_sending)
    {
      hott_check_serial_data();           // Check for data request
    }

  if(hott_msg_ptr == 0) return;       // No data to send

  //  pc.printf("%s\n","YO");

  if(hott_telemetry_is_sending)
    {
      if(tnow - hott_serial_timer < HOTT_TX_DELAY_US)
        {
          return;
        }
    }

  hott_send_telemetry_data();
  hott_serial_timer = tnow;
}

void hott_check_serial_data()
{
  if(hott_telemetry_is_sending == true)
    {
      return;
    }

  uint8_t bytes_waiting = hott_port_available();

  if(bytes_waiting <= 1)
    {
      return;
    }

  if(bytes_waiting > 2)
    {
      hott_port_flush();
      looking_for_request = true;
      return;
    }

  if(looking_for_request)
    {
      last_hott_check_request = tnow;
      looking_for_request = false;
      return;
    }
  else
    {
      bool enough_time_passed = (tnow - last_hott_check_request) >= HOTT_RX_SCHEDULE;
      if(!enough_time_passed)
        {
          return;
        }
      looking_for_request = true;
    }

  unsigned char c = hott_port_read();
  unsigned char addr = hott_port_read();
  //  pc.printf("%s %x %x\n","read_request:", c, addr);
  // Ok, we have a valid request, check for address
  switch(c)
    {
    case HOTT_TEXT_MODE_REQUEST_ID:
      {
        hott_txt_msg.start_byte = 0x7b;
        hott_txt_msg.stop_byte = 0x7d;
        if((addr >> 4) == (HOTT_SIM_TEXTMODE_ADDRESS & 0x0f))
          {
            hott_clear_text_screen();
            hott_txt_msg.fill1 = (addr >> 4);
            //                    HOTT_PrintWord_P((21*1), PSTR(THISFIRMWARE),0 );
            //            char* test = "------ CREDTIS ------";
            char* test = "------ RC_PARK ------";
            HOTT_PrintWord((21*3), test ,0 );
            HOTT_PrintWord((21*3), test ,0 );
            HOTT_PrintWord((21*4), test ,0 );
            HOTT_PrintWord((21*5), test ,0 );
            HOTT_PrintWord((21*6), test ,0 );
            hott_send_text_msg();
          }
        break;
      }
      
    case HOTT_BINARY_MODE_REQUEST_ID:
      {
        if(addr == HOTT_TELEMETRY_EAM_SENSOR_ID)
          {
            hott_send_eam_msg();
            HOTT_REQ_UPDATE_EAM = true;
            break;
          }
      }
    default:
      break;
    }
}

int hott_port_available()
{
  return (rx_in >= rx_out ? rx_in - rx_out : rx_in + rx_buffer_size - rx_out);
}

// Read a bit from the large rx buffer from rx interrupt routine
uint32_t hott_port_read()
{
  uint32_t temp = rx_buffer[rx_out];
  rx_out = (rx_out + 1) % rx_buffer_size;
  return temp;
}

void hott_port_flush()
{
  rx_out = rx_in;
}

void hott_clear_text_screen()
{
  memset(hott_txt_msg.msg_txt, 0x20, sizeof(hott_txt_msg.msg_txt));
}

void hott_send_text_msg()
{
  hott_send_msg((uint8_t *)&hott_txt_msg, sizeof(struct HOTT_TEXTMODE_MSG));
}

void hott_send_eam_msg()
{
  hott_send_msg((uint8_t *)&hott_eam_msg, sizeof(hott_eam_msg));
}

void hott_update_eam_msg()
{
  hott_eam_msg.batt1_voltage_L = 0;
  hott_eam_msg.batt2_voltage_L = 0;
  hott_eam_msg.temp1 = 30; //(int8_t)((barometer.get_temperature() / 10) + 20);
  hott_eam_msg.temp2 = 20;	//0°
  hott_eam_msg.altitude_L = 200; //(int)((current_loc.alt - home.alt) / 100)+500;
  hott_eam_msg.current_L = 100; //current_amps1*10;
  hott_eam_msg.main_voltage_L = 40; //(int)(battery_voltage1 * 10.0);
  hott_eam_msg.batt_cap_L = 60; //current_total1 / 10;
  hott_eam_msg.speed_L = 10; //(int)((float)(g_gps->ground_speed * 0.036));

  hott_eam_msg.climbrate_L = 30; //30000 + climb_rate;  
  hott_eam_msg.climbrate3s = 120; //120 + (climb_rate / 100);  // 0 m/3s using filtered data here

  hott_eam_msg.alarm_invers2 &= 0x7f;
}

void hott_update_telemetry_data()
{
  // No update while sending
  if((hott_telemetry_sendig_msgs_id != HOTT_TELEMETRY_EAM_SENSOR_ID) && HOTT_REQ_UPDATE_EAM == true)
  {
	hott_update_eam_msg();
	HOTT_REQ_UPDATE_EAM = false;
  }
}

void hott_send_msg(uint8_t *buffer, int len)
{
  if(hott_telemetry_is_sending == true) return;
  hott_msg_ptr = buffer;
  hott_msg_len = len + HOTT_CRC_SIZE; // len + 1 byte for crc
  hott_telemetry_sendig_msgs_id = buffer[1];   // HoTT msgs id is the 2. byte
}


// Transmitts a HoTT message
void hott_send_telemetry_data()
{
  if(!hott_telemetry_is_sending)
    {
      // New message to send
      hott_telemetry_is_sending = true;
      hott_enable_transmitter();  // Switch to transmit mode
      msg_crc = 0;
      return;
    }

  if(hott_msg_len == 0)
    {
      // All data sent
      hott_msg_ptr = NULL;
      hott_telemetry_is_sending = false;
      hott_telemetry_sendig_msgs_id = 0; // Clear current hott msg id
      hott_enable_receiver();
      hott_port_flush();
      return;
    }
  
  --hott_msg_len;
  if(hott_msg_len == 0)
    {
      hott_port_write(msg_crc++);
      return;
    }

  msg_crc += *hott_msg_ptr;
  //          pc.printf("%x\n",*hott_msg_ptr);
  hott_port_write(*hott_msg_ptr++);
}

// Switch to transmit mode
void hott_enable_transmitter()
{
  uart.attach(NULL, RawSerial::RxIrq);
  uart.attach(&Tx_interrupt, RawSerial::TxIrq);
}

// Switch to receive mode
void hott_enable_receiver()
{
  uart.attach(&Rx_interrupt, RawSerial::RxIrq);
  uart.attach(NULL, RawSerial::TxIrq);
}


void hott_port_write(uint8_t data_to_send)
{
  if (uart.writeable())
    {
      uart.putc(data_to_send);
    }
}

/*
// Copy tx line buffer to large tx buffer for tx interrupt routine
void hott_port_write()
{
  char temp_char;
  bool empty;
  // Start Critical Section - don't interrupt while changing global buffer variables
  empty = (tx_in == tx_out);
  // Wait if buffer is full
  if (((tx_in + 1) % tx_buffer_size) == tx_out)
    {
      // End Critical Section - need to let interrupt routine empty buffer by sending
      uart.attach(&Tx_interrupt, RawSerial::TxIrq);
      while (((tx_in + 1) % tx_buffer_size) == tx_out) {}
      // Start Critical Section - don't interrupt while changing global buffer variables
      uart.attach(NULL, RawSerial::TxIrq);
    }
  
  int i = 0;
  while(i < i_line)
    {
      tx_buffer[tx_in] = tx_line[i];
      i++;
      tx_in = (tx_in + 1) % tx_buffer_size;
    }
  
  if (uart.writeable() && (empty)) {
    temp_char = tx_buffer[tx_out];
    tx_out = (tx_out + 1) % tx_buffer_size;
    // Send first character to start tx interrupts, if stopped
    uart.putc(temp_char);
  }
  // End Critical Section
  return;
}
*/



// Printing to Textframe
void  HOTT_PrintWord(uint8_t pos,  char *w, bool inverted) {
  for (uint8_t index = 0; ; index++) {
    if (w[index] == 0x0) {
      break;
    } else {
      hott_txt_msg.msg_txt[index+pos] = inverted ? w[index] +128: w[index];
    }
  }
}
