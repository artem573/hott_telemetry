#include "pins.h"

RawSerial uart(PA_9, PA_10); // tx, rx
Serial pc(USBTX, USBRX);

Ticker ticker; // !! max ~30 [min] <- 32-bit [us] counter
Timer timer; // !! max ~30 [min] <- 32-bit [us] counter

PpmIn p1(PB_10);
