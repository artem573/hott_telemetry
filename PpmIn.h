#pragma once

#include "mbed.h"

class PpmIn
{
public:
  InterruptIn _p;
  Timer _t;
  double _period, _pulsewidth;
  double _channels[8];
  int _channel;

  PpmIn(PinName p);
  void rise();
  void fall();

  void disable_interrupt();
  void enable_interrupt();
};
