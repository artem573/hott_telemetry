#include "PpmIn.h"


PpmIn::PpmIn(PinName p):
  _p(p)
{
  _p.rise(this, &PpmIn::rise);
  _p.fall(this, &PpmIn::fall);
  _period = 0.0;
  _t.start();
  _channel = 0;
}

void PpmIn::rise()
{
  _period = _t.read();
  if(_period > 0.005) _channel = 0;
  else
    {
      _channels[_channel] = _period;
      _channel ++;
    }
  _t.reset();
}

void PpmIn::fall()
{
}

void PpmIn::disable_interrupt()
{
  _p.rise(NULL);
  _p.fall(NULL);
}

void PpmIn::enable_interrupt()
{
  _p.rise(this, &PpmIn::rise);
  _p.fall(this, &PpmIn::fall);
}

