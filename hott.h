#pragma once

#include "mbed.h"
#include "pins.h"

#define HOTT_MESSAGE_PREPARATION_FREQUENCY_5_HZ ((1000 * 1000) / 5)
#define HOTT_RX_SCHEDULE 4000
#define HOTT_TX_DELAY_US 3000

#define HOTT_TEXT_MODE_REQUEST_ID       0x7f
#define HOTT_BINARY_MODE_REQUEST_ID     0x80

//Graupner #33600 GPS Module
#define HOTT_TELEMETRY_GPS_SENSOR_ID    0x8a
//Graupner #33620 Electric Air Module
#define HOTT_TELEMETRY_EAM_SENSOR_ID    0x8e
//Textmode address to simulate (GPS)
#define HOTT_SIM_TEXTMODE_ADDRESS       HOTT_TELEMETRY_GPS_SENSOR_ID

#define HOTT_TEXTMODE_MSG_TEXT_LEN 168

// Text mode msgs type
struct HOTT_TEXTMODE_MSG
{
  uint8_t start_byte;      //#01 constant value 0x7b
  uint8_t fill1;           //#02 constant value 0x00
  uint8_t warning_beeps;   //#03 1=A 2=B ...
  uint8_t msg_txt[HOTT_TEXTMODE_MSG_TEXT_LEN];     //#04 ASCII text to display to
  // Bit 7 = 1 -> Inverse character display
  // Display 21x8
  uint8_t stop_byte;               //#171 constant value 0x7d
  //      int8_t parity;          //#172 Checksum / parity
};

typedef struct HOTT_EAM_MSG_s {
    uint8_t start_byte;          //#01 start uint8_t
    uint8_t eam_sensor_id;       //#02 EAM sensort id. constat value 0x8e
    uint8_t warning_beeps;           //#03 1=A 2=B ... or 'A' - 0x40 = 1
                                // Q    Min cell voltage sensor 1
                                // R    Min Battery 1 voltage sensor 1
                                // J    Max Battery 1 voltage sensor 1
                                // F    Mim temperature sensor 1
                                // H    Max temperature sensor 1
                                // S    Min cell voltage sensor 2
                                // K    Max cell voltage sensor 2
                                // G    Min temperature sensor 2
                                // I    Max temperature sensor 2
                                // W    Max current
                                // V    Max capacity mAh
                                // P    Min main power voltage
                                // X    Max main power voltage
                                // O    Min altitude
                                // Z    Max altitude
                                // C    (negative) sink rate m/sec to high
                                // B    (negative) sink rate m/3sec to high
                                // N    climb rate m/sec to high
                                // M    climb rate m/3sec to high

    uint8_t sensor_id;           //#04 constant value 0xe0
    uint8_t alarm_invers1;       //#05 alarm bitmask. Value is displayed inverted
                                //Bit#  Alarm field
                                // 0    mAh
                                // 1    Battery 1
                                // 2    Battery 2
                                // 3    Temperature 1
                                // 4    Temperature 2
                                // 5    Altitude
                                // 6    Current
                                // 7    Main power voltage
    uint8_t alarm_invers2;       //#06 alarm bitmask. Value is displayed inverted
                                //Bit#  Alarm Field
                                // 0    m/s
                                // 1    m/3s
                                // 2    Altitude (duplicate?)
                                // 3    m/s (duplicate?)
                                // 4    m/3s (duplicate?)
                                // 5    unknown/unused
                                // 6    unknown/unused
                                // 7    "ON" sign/text msg active

    uint8_t cell1_L;             //#07 cell 1 voltage lower value. 0.02V steps, 124=2.48V
    uint8_t cell2_L;             //#08
    uint8_t cell3_L;             //#09
    uint8_t cell4_L;             //#10
    uint8_t cell5_L;             //#11
    uint8_t cell6_L;             //#12
    uint8_t cell7_L;             //#13
    uint8_t cell1_H;             //#14 cell 1 voltage high value. 0.02V steps, 124=2.48V
    uint8_t cell2_H;             //#15
    uint8_t cell3_H;             //#16
    uint8_t cell4_H;             //#17
    uint8_t cell5_H;             //#18
    uint8_t cell6_H;             //#19
    uint8_t cell7_H;             //#20

    uint8_t batt1_voltage_L;     //#21 battery 1 voltage lower value in 100mv steps, 50=5V. optionally cell8_L value 0.02V steps
    uint8_t batt1_voltage_H;     //#22

    uint8_t batt2_voltage_L;     //#23 battery 2 voltage lower value in 100mv steps, 50=5V. optionally cell8_H value. 0.02V steps
    uint8_t batt2_voltage_H;     //#24

    uint8_t temp1;               //#25 Temperature sensor 1. 20=0�, 46=26� - offset of 20.
    uint8_t temp2;               //#26 temperature sensor 2

    uint8_t altitude_L;          //#27 Attitude lower value. unit: meters. Value of 500 = 0m
    uint8_t altitude_H;          //#28

    uint8_t current_L;           //#29 Current in 0.1 steps
    uint8_t current_H;           //#30

    uint8_t main_voltage_L;      //#31 Main power voltage (drive) in 0.1V steps
    uint8_t main_voltage_H;      //#32

    uint8_t batt_cap_L;          //#33 used battery capacity in 10mAh steps
    uint8_t batt_cap_H;          //#34

    uint8_t climbrate_L;         //#35 climb rate in 0.01m/s. Value of 30000 = 0.00 m/s
    uint8_t climbrate_H;         //#36

    uint8_t climbrate3s;         //#37 climbrate in m/3sec. Value of 120 = 0m/3sec

    uint8_t rpm_L;               //#38 RPM. Steps: 10 U/min
    uint8_t rpm_H;               //#39

    uint8_t electric_min;        //#40 Electric minutes. Time does start, when motor current is > 3 A
    uint8_t electric_sec;        //#41

    uint8_t speed_L;             //#42 (air?) speed in km/h. Steps 1km/h
    uint8_t speed_H;             //#43
    uint8_t stop_byte;           //#44 stop uint8_t
} HOTT_EAM_MSG_t;

void Rx_interrupt();
void Tx_interrupt();
void hott_enable_transmitter();  
void hott_enable_receiver(); 
void hott_serial_scheduler();
void hott_check_serial_data();
int hott_port_available();
uint32_t hott_port_read();
void hott_port_write(uint8_t);
void hott_port_flush();
void hott_clear_text_screen();
void hott_send_text_msg();
void hott_send_eam_msg();
void hott_update_eam_msg();
void hott_update_telemetry_data();
void hott_send_msg(uint8_t *buffer, int len);
void hott_send_telemetry_data();
void HOTT_PrintWord(uint8_t pos, char *w, bool inverted);
void init_telemetry();
