#pragma once

#include "mbed.h"
#include "PpmIn.h"

extern RawSerial uart; // tx, rx
extern Serial pc;

extern Ticker ticker; // !! max ~30 [min] <- 32-bit [us] counter
extern Timer timer; // !! max ~30 [min] <- 32-bit [us] counter

extern PpmIn p1;
